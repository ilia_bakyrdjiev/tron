﻿var snake;
var apple;
var map;
var snakes = [];
var connection;
var currentSnakeId;

window.onload = function () {
    canv = document.getElementById("gc");
    ctx = canv.getContext("2d");
    document.addEventListener("keydown", keyPush);
    map = new Map();
    snake = new Snake();
    apple = new Apple();

    connection = new signalR.HubConnectionBuilder().withUrl("/messageHub").build();

    connection.on("PingSnakes", function (data) {
        snakes = data;
    });

    connection.on("GameOver", function (snakeId) {
        if (currentSnakeId == snakeId) {
            alert('Game over, please refresh');
            location.reload();
        }
    })

    connection.on("PopulateApple", function (data) {
            apple.x = data.x;
            apple.y = data.y;
    })

    connection.start().then(function () {
        snake.id = genrateGuid();
        currentSnakeId = snake.id;
        connection.invoke("ConnectSnake", snake).catch(err => console.error(err.toString()));
    }).catch(function (err) {
        return console.error(err.toString());
    });

    function genrateGuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    setInterval(game, 1000 / 15);
}

window.onbeforeunload = function (e) {
    connection.invoke("OnDisconnected", snake).catch(err => console.error(err.toString()));
};

function Snake() {
    this.id = "";
    this.x = Math.floor(Math.random() * map.tilecount);
    this.y = Math.floor(Math.random() * map.tilecount);
    this.xvel = 0;
    this.yvel = 0;
    this.tail = 2;
    this.trail = [];
}

Snake.prototype.eat = function () {
    this.tail++;
}

Snake.prototype.die = function () {
    this.tail = 5;
}

function Map() {
    this.gridsize = 20;
    this.tilecount = 20;
}

function Apple(x, y) {
    this.x = x;
    this.y = y;
}

function game() {
    snake.x += snake.xvel;
    snake.y += snake.yvel;

    if (snake.x < 0) {
        snake.x = map.tilecount - 1;
    }
    if (snake.x > map.tilecount - 1) {
        snake.x = 0;
    }
    if (snake.y < 0) {
        snake.y = map.tilecount - 1;
    }
    if (snake.y > map.tilecount - 1) {
        snake.y = 0;
    }
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canv.width, canv.height);

    for (var i = 0; i < snakes.length; i++) {
        var current = snakes[i];
        for (var k = 0; k < current.trail.length; k++) {
            ctx.fillStyle = "lime";
            ctx.fillRect(current.trail[k].x * map.gridsize, current.trail[k].y * map.gridsize, map.gridsize - 2, map.gridsize - 2);
        }
    }

    ctx.fillStyle = "orange";
    for (var i = 0; i < snake.trail.length; i++) {
        ctx.fillRect(snake.trail[i].x * map.gridsize, snake.trail[i].y * map.gridsize, map.gridsize - 2, map.gridsize - 2);
        if (snake.trail[i].x == snake.x && snake.trail[i].y == snake.y) {
            snake.tail = 5;
        }
    }

    snake.trail.push({ x: snake.x, y: snake.y });
    while (snake.trail.length > snake.tail) {
        snake.trail.shift();
    }

    connection.invoke("OnMove", snake).catch(err => console.error(err.toString()));

    if (apple.x == snake.x && apple.y == snake.y) {
        snake.tail++;
        var data = generateRandomPoint();
        connection.invoke("GetAppleCordinates", data).catch(err => console.error(err.toString()));
    }
    ctx.fillStyle = "red";
    ctx.fillRect(apple.x * map.gridsize, apple.y * map.gridsize, map.gridsize - 2, map.gridsize - 2);
}

function generateRandomPoint() {
    var data = {
        x: Math.floor(Math.random() * map.tilecount),
        y: Math.floor(Math.random() * map.tilecount)
    }

    return data;
}


function keyPush(evt) {
    switch (evt.keyCode) {
        case 37:
            snake.xvel = -1; snake.yvel = 0;
            break;
        case 38:
            snake.xvel = 0; snake.yvel = -1;
            break;
        case 39:
            snake.xvel = 1; snake.yvel = 0;
            break;
        case 40:
            snake.xvel = 0; snake.yvel = 1;
            break;
    }
}