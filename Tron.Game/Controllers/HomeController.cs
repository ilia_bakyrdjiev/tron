﻿namespace Tron.Game
{
    using Microsoft.AspNetCore.Mvc;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Linq;
    using Tron.Core.Dto;
    using Tron.Core.Interfaces;

    public class HomeController : Controller
    {
        private readonly IGameManager gameManager;
        private readonly IDynamicSender dynamicSender;

        public HomeController(IGameManager gameManager, IDynamicSender dynamicSender)
        {
            this.dynamicSender = dynamicSender;
            this.gameManager = gameManager;
        }

        private void Init()
        {
            this.gameManager.Initilize(Callback);
        }

        private void Callback(object state)
        {
            var snakes = this.gameManager.GetSnakes();
            List<Snake> result = snakes.Values.ToList();
            this.dynamicSender.Ping(result);
        }

        public IActionResult Index()
        {
            this.Init();
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}