﻿namespace Tron.Game
{
    using Microsoft.AspNetCore.SignalR;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Tron.Core.Dto;
    using Tron.Core.Interfaces;

    public class SnakeMessageHub : Hub, IDynamicSender
    {
        private IGameManager gameManager;
        private IHubContext<SnakeMessageHub> hubContext;

        public SnakeMessageHub(IGameManager gameManager, IHubContext<SnakeMessageHub> hubContext)
        {
            this.hubContext = hubContext;
            this.gameManager = gameManager;
        }

        public async Task ConnectSnake(Snake snake)
        {
            var result = this.gameManager.GetSnake(snake.Id);
            if (result == null)
                this.gameManager.AddSnake(snake);

            await this.Ping(new List<Snake>() { snake });
        }

        public async Task Ping(List<Snake> snakes)
        {
            await this.hubContext.Clients.All.SendAsync("PingSnakes", snakes);
        }

        public void OnMove(Snake snake)
        {
            var result = this.gameManager.GetSnake(snake.Id);
            if (result != null)
            {
                result.X = snake.X;
                result.Y = snake.Y;
                result.Tail = snake.Tail;
                result.Trail = snake.Trail;
                result.Tail = snake.Tail;
            }

            bool hasColison = gameManager.DetectCollision(snake);
            if (hasColison)
            {
                this.hubContext.Clients.All.SendAsync("GameOver", snake.Id).Wait();
                this.gameManager.RemoveSnake(snake.Id);
            }
        }

        public void OnDisconnected(Snake snake)
        {
            this.gameManager.RemoveSnake(snake.Id);
        }

        public async Task GetAppleCordinates(Point point)
        {
            await this.hubContext.Clients.All.SendAsync("PopulateApple", point);
        }
    }
}