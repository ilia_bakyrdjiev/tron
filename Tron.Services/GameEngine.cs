﻿namespace Tron.Services
{
    using System.Collections.Concurrent;
    using System.Linq;
    using System.Threading;
    using Tron.Core.Dto;
    using Tron.Core.Interfaces;

    public class GameEnigne : IGameManager
    {
        public GameEnigne()
        {
        }

        public static ConcurrentDictionary<string, Snake> Snakes { get; set; }

        public ConcurrentDictionary<string, Snake> GetSnakes()
        {
            return Snakes;
        }

        public Timer Timer;

        public void Initilize(TimerCallback callback)
        {
            if (Snakes == null)
            {
                Snakes = new ConcurrentDictionary<string, Snake>();
            }

            Timer = new Timer(callback, null, 0, 1000 / 15);
        }

        public Snake GetSnake(string id)
        {
            Snake snake = null;
            Snakes.TryGetValue(id, out snake);
            return snake;
        }

        public void AddSnake(Snake snake)
        {
            Snakes.TryAdd(snake.Id, snake);
        }

        public void RemoveSnake(string snakeId)
        {
            Snakes.TryRemove(snakeId, out Snake pew);
        }

        public bool DetectCollision(Snake snake)
        {
            var allSnakes = Snakes.Values.Where(s=> s.Id != snake.Id).ToList();
            foreach (var currentSnake in allSnakes)
            {
                var trail = currentSnake.Trail;
                foreach (var element in trail)
                {
                    if (element.X == snake.X && element.Y == snake.Y)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}