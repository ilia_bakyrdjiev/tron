﻿namespace Tron.Core.Dto
{
    public class Point
    {
        public int X { get; set; }

        public int Y { get; set; }
    }
}
