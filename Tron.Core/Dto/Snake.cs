﻿namespace Tron.Core.Dto
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Snake : Point
    {
        public string Id { get; set; }

        public int Tail { get; set; }

        public List<Point> Trail { get; set; }
    }
}
