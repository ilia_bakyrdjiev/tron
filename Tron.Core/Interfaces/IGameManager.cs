﻿namespace Tron.Core.Interfaces
{
    using System.Collections.Concurrent;
    using System.Threading;
    using Tron.Core.Dto;

    public interface IGameManager
    {
        void Initilize(TimerCallback callback);

        Snake GetSnake(string id);

        void AddSnake(Snake snake);

        void RemoveSnake(string socketId);

        ConcurrentDictionary<string, Snake> GetSnakes();

        bool DetectCollision(Snake snake);
    }
}