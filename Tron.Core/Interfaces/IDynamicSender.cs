﻿namespace Tron.Core.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Tron.Core.Dto;

    public interface IDynamicSender
    {
        Task Ping(List<Snake> snakes);

        Task ConnectSnake(Snake snake);
    }
}